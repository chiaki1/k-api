import { Module } from '@nestjs/common';
import { TestMongodbService } from './test-mongodb.service';
import { Cat, CatSchema } from '@libs/test-mongodb/schemas/cat.schema';
import { MongooseModule } from '@nestjs/mongoose';
import { TestMongodbController } from '@libs/test-mongodb/controllers/test-mongodb';
import { CatsService } from '@libs/test-mongodb/services/cat.service';

@Module({
  imports: [MongooseModule.forFeature([{ name: Cat.name, schema: CatSchema }])],
  providers: [TestMongodbService, CatsService],
  controllers: [TestMongodbController],
  exports: [TestMongodbService, CatsService],
})
export class TestMongodbModule {}
