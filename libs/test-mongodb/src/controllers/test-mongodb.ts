import { Controller, Get } from '@nestjs/common';
import { CatsService } from '@libs/test-mongodb/services/cat.service';
import { Cat } from '@libs/test-mongodb/schemas/cat.schema';

@Controller('cats')
export class TestMongodbController {
  constructor(private catsService: CatsService) {}

  @Get('save-cat')
  async create(): Promise<Cat> {
    return this.catsService.create({
      name: 'Cat ' + Math.random() * 10000,
      age: 10,
    });
  }

  @Get('get-cat')
  async findAll(): Promise<Cat[]> {
    return await this.catsService.findAll();
  }
}
