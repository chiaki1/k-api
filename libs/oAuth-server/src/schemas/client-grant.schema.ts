import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class ClientGrant extends Document {
  @Prop({
    type: String,
    required: true,
  })
  type: string;

  @Prop({
    type: String,
    required: true,
  })
  clientId: string;
}

export const ClientGrantSchema = SchemaFactory.createForClass(ClientGrant);
