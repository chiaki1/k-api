import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class Token extends Document {
  @Prop({
    type: String,
    required: true,
  })
  accessToken: string;

  @Prop({
    type: String,
  })
  authorizationCode?: string;

  @Prop({
    type: Date,
    required: true,
  })
  accessTokenExpiresAt: Date;

  @Prop({
    type: String,
  })
  refreshToken?: string;

  @Prop({
    type: Date,
    required: true,
  })
  refreshTokenExpiresAt: Date;

  @Prop({
    type: String,
  })
  scope: string;

  @Prop({
    type: String,
    required: true,
  })
  userId: string;

  @Prop({
    type: String,
    required: true,
  })
  clientId: string;

  [prop: string]: any;
}

export const TokenSchema = SchemaFactory.createForClass(Token);
