import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class User extends Document {
  @Prop({
    type: String,
  })
  phone: string;

  @Prop({
    type: String,
  })
  username?: string;

  @Prop({
    type: String,
  })
  password?: string;

  @Prop({
    type: Date,
    default: new Date(),
  })
  createdAt?: Date;

  @Prop({
    type: Date,
    default: new Date(),
  })
  lastModifiedAt?: Date;
}

export const UserSchema = SchemaFactory.createForClass(User);
