import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class Client extends Document {
  @Prop({
    type: String,
    required: true,
  })
  clientId: string;

  @Prop({
    type: String,
    required: true,
  })
  clientSecret: string;

  @Prop({
    type: [String],
  })
  redirectUris?: [string];
}

export const ClientSchema = SchemaFactory.createForClass(Client);
