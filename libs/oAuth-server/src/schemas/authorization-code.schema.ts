import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class AuthorizationCode extends Document {
  @Prop({
    type: String,
    required: true,
  })
  authorizationCode: string;

  @Prop({
    type: Date,
    required: true,
  })
  expiresAt: Date;

  @Prop({
    type: String,
  })
  redirectUri?: string;

  @Prop({
    type: String,
  })
  scope: string;

  @Prop({
    type: String,
    required: true,
  })
  userId: string;

  @Prop({
    type: String,
    required: true,
  })
  clientId: string;
}

export const AuthorizationCodeSchema = SchemaFactory.createForClass(
  AuthorizationCode,
);
