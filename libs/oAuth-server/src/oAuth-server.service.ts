import {
  BadRequestException,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { AuthorizationCode, Client, ClientGrant, Token, User } from './schemas';
import { Model } from 'mongoose';
import _ from 'lodash';
import OAuth2Server = require('oauth2-server');
const md5 = require('md5');

@Injectable()
export class OAuthServerService {
  static DEFAULT_URI = 'https://ggg.com.vn';
  constructor(
    @InjectModel(User.name) private userModel: Model<User>,
    @InjectModel(AuthorizationCode.name)
    private authorizationCodeModel: Model<AuthorizationCode>,
    @InjectModel(Client.name) private clientModel: Model<Client>,
    @InjectModel(ClientGrant.name) private clientGrantModel: Model<ClientGrant>,
    @InjectModel(Token.name) private tokenModel: Model<Token>,
  ) {}

  protected _instance: OAuth2Server;

  getOAuthServer(): OAuth2Server {
    if (!this._instance) {
      this._instance = new OAuth2Server({
        model: this.model(),
      });
    }
    return this._instance;
  }

  generateRandomAuthorizationCode(length = 4) {
    let output = '';
    const options = '0123456789'; // number only
    for (let i = 0; i < length; i++) {
      output += options.charAt(Math.floor(Math.random() * options.length));
    }
    return output;
  }

  async authorize(
    request: OAuth2Server.Request,
    response: OAuth2Server.Response,
    authenticateHandler: any,
  ) {
    try {
      return await this.getOAuthServer().authorize(request, response, {
        authenticateHandler,
        allowEmptyState: true,
        authorizationCodeLifetime: 30000, // 5 minutes
      });
    } catch (e) {
      throw new BadRequestException(e);
    }
  }

  async token(request: OAuth2Server.Request, response: OAuth2Server.Response) {
    try {
      return await this.getOAuthServer().token(request, response, {
        // Allow token requests using the password grant to not include a client_secret.
        requireClientAuthentication: {
          password: false,
          authorization_code: false,
        },
      });
    } catch (e) {
      throw new BadRequestException(e);
    }
  }

  async authenticate(
    request: OAuth2Server.Request,
    response: OAuth2Server.Response,
  ): Promise<any> {
    try {
      return await this.getOAuthServer().authenticate(request, response);
    } catch (e) {
      throw new UnauthorizedException();
    }
  }

  protected model(): any {
    return {
      getAccessToken: async (access_token: string) => {
        const accessToken = await this.tokenModel.findOne({
          accessToken: access_token,
        });

        if (access_token) {
          const user = await this.userModel.findById(accessToken.userId);

          if (!user) {
            return false;
          }

          accessToken.user = user;

          return accessToken;
        }

        return false;
      },

      getClient: async (clientId: string, clientSecret: string) => {
        if (!clientSecret) {
          const buff = new Buffer(clientId, 'base64');
          const idSecretStr = buff.toString('ascii');
          const idSecretArr = idSecretStr.split(':');
          if (_.size(idSecretArr) === 2) {
            clientId = idSecretArr[0];
            clientSecret = idSecretArr[1];
          }
        }

        const client = await this.clientModel.findOne({
          clientId,
          clientSecret,
        });
        // .exec();

        let grants = await this.clientGrantModel.find({ clientId });
        grants = _.map(grants, grant => grant.type);

        if (client) {
          return {
            id: clientId,
            clientId,
            grants,
            redirectUris:
              client.redirectUris && !_.isEmpty(client.redirectUris)
                ? client.redirectUris
                : [OAuthServerService.DEFAULT_URI],
          };
        }

        return false;
      },

      getRefreshToken: async (refreshToken: string) => {
        const token = await this.tokenModel.findOne({ refreshToken }).exec();
        console.log(token);
        return token;
      },

      /*
       * Su dung trong Password Grant de verify xem nguoi  dung co ton tai  khong
       * */
      getUser: async (username: string, password: string) => {
        const user = await this.userModel.findOne({
          username,
          password: md5(password),
        });

        if (user) {
          return {
            id: user.id,
            userId: user.id,
            phone: user.phone,
          };
        }
        return false;
      },

      saveToken: async (token: any, client: any, user: any) => {
        const tokenDto = {
          ...token,
          clientId: client.clientId,
          userId: user.id,
        };

        await this.tokenModel.create(tokenDto);

        return {
          ...tokenDto,
          client,
          user,
        };
      },

      // when user get new token
      revokeToken: async (token: any) => {
        return await this.tokenModel
          .deleteMany({ accessToken: token.accessToken })
          .exec();
      },

      // generateAuthorizationCode: async (client: any, user: any, scope: any) => {
      // },

      saveAuthorizationCode: async (code: any, client: any, user: any) => {
        let randomCode: string;
        // TODO: bypass OTP
        // eslint-disable-next-line prefer-const
        // randomCode = !!_.isString(user['phone']) ? user.phone.slice(-4) : generateRandomAuthorizationCode();
        // eslint-disable-next-line prefer-const
        randomCode = this.generateRandomAuthorizationCode();

        code = { ...code, authorizationCode: randomCode };
        const authorizeCodeDto = {
          ...code,
          clientId: client.clientId,
          userId: user.id,
        };
        /**
         * Nếu không truyền redirect_uri trong request body thì mặc định sẽ lấy phần từ đầu tiên của client.redirectUris
         */
        await this.authorizationCodeModel.create(authorizeCodeDto);
        return {
          ...authorizeCodeDto,
          client,
          user,
        };
      },

      getAuthorizationCode: async (authorizationCode: string) => {
        const code = await this.authorizationCodeModel.findOne({
          authorizationCode,
        });

        if (code) {
          const user = await this.userModel.findById(code.userId).exec();
          const client = await this.clientModel.findOne({
            clientId: code.clientId,
          });

          return _.merge(code.toObject(), {
            // @ts-ignore fix error in node_modules/oauth2-server/lib/grant-types/authorization-code-grant-type.js:106
            client: {
              id: client.clientId,
              clientId: client.clientId,
            },
            user: {
              id: user._id,
              phone: user.phone,
            },
            code: code.authorizationCode,
            redirectUri: false, // bypass check redirectURI
          });
        }

        return false;
      },

      revokeAuthorizationCode: async (code: any) => {
        return await this.authorizationCodeModel
          .deleteMany({
            authorizationCode: code.authorizationCode,
          })
          .exec();
      },
    };
  }
}
