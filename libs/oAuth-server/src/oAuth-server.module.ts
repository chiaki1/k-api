import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import {
  AuthorizationCode,
  AuthorizationCodeSchema,
  Client,
  ClientGrant,
  ClientGrantSchema,
  ClientSchema,
  Token,
  TokenSchema,
  User,
  UserSchema,
} from './schemas';
import { OAuthController } from './controllers';
import { OAuthServerService } from './oAuth-server.service';
import { PassportModule } from '@nestjs/passport';
import { LocalAuthGuard } from './guards';
import { LocalStrategy } from './passport-strategies';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: User.name, schema: UserSchema }]),
    MongooseModule.forFeature([{ name: Token.name, schema: TokenSchema }]),
    MongooseModule.forFeature([
      { name: ClientGrant.name, schema: ClientGrantSchema },
    ]),
    MongooseModule.forFeature([{ name: Client.name, schema: ClientSchema }]),
    MongooseModule.forFeature([
      { name: AuthorizationCode.name, schema: AuthorizationCodeSchema },
    ]),

    PassportModule,
  ],
  controllers: [OAuthController],
  providers: [OAuthServerService, LocalStrategy, LocalAuthGuard],
  exports: [OAuthServerService],
})
export class OAuthServerModule {}
