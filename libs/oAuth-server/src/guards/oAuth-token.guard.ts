import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import OAuth2Server from 'oauth2-server';
import { OAuthServerService } from '../oAuth-server.service';

@Injectable()
export class OAuthTokenGuard implements CanActivate {
  constructor(private oAuthServerService: OAuthServerService) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const { Request, Response } = OAuth2Server;
    const res = context.switchToHttp().getResponse<Response>();
    const req = context.switchToHttp().getRequest<Request>();

    return !!(await this.oAuthServerService.authenticate(
      new Request(req),
      new Response(res),
    ));
  }
}
