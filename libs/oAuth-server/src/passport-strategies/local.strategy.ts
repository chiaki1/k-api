import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-local';
import { InjectModel } from '@nestjs/mongoose';
import { User } from '@libs/oAuth-server/schemas';
import { Model } from 'mongoose';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy, 'local') {
  constructor(@InjectModel(User.name) private userModel: Model<User>) {
    super();
  }

  async validate(username: string, password: string): Promise<any> {
    const user = await this.userModel.findOne({ username, password });
    if (!user) {
      throw new UnauthorizedException();
    }
    return user;
  }
}
