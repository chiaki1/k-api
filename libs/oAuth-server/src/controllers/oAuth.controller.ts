import {
  BadRequestException,
  Body,
  Controller,
  Get,
  Post,
  Req,
  Res,
  UseGuards,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import {
  AuthorizationCode,
  Client,
  ClientGrant,
  Token,
  User,
} from './../schemas';
import { Model } from 'mongoose';
import { Request, Response } from 'express';
import { OAuthServerService } from './../oAuth-server.service';
import OAuth2Server from 'oauth2-server';
import { LocalAuthGuard } from '../guards';
import { OAuthTokenGuard } from '@libs/oAuth-server/guards/oAuth-token.guard';

@Controller('oauth')
export class OAuthController {
  constructor(
    @InjectModel(User.name) private userModel: Model<User>,
    @InjectModel(AuthorizationCode.name)
    private authorizationCodeModel: Model<AuthorizationCode>,
    @InjectModel(Client.name) private clientModel: Model<Client>,
    @InjectModel(ClientGrant.name) private clientGrantModel: Model<ClientGrant>,
    @InjectModel(Token.name) private tokenModel: Model<Token>,
    private oAuth2Server: OAuthServerService,
  ) {}

  /**
   * Lấy authorizationCode bằng số điện thoại
   * Lưu ý function này bypass authenticate bởi vì người dùng sẽ nhận được call/sms để chứng thực
   *
   * @param request
   * @param response
   * @param data
   * @returns {Promise<e.Response<any>>}
   */
  @Post('phone/authorize')
  async phoneAuthorize(
    @Req() request: Request,
    @Res() response: Response,
    @Body()
    data: {
      phone: string;
    },
  ) {
    if (!data || !data.phone) {
      throw new BadRequestException(undefined, 'Missing `phone` data');
    }

    /**
     * Vì sử dụng call/sms để đăng nhập nên có thể bypass authenticate
     * @type {{handle: () => Promise<{phone: string, id: any, userId: any} | User>}}
     */
    const authenticateHandler = {
      handle: async () => {
        const user = await this.userModel.findOne({ phone: data.phone }).exec();
        if (user != null) {
          return { phone: user.phone, id: user.id, userId: user.id };
        }
        // If user not exist, try to insert new one
        const phone = data.phone;
        return await this.userModel.create({
          phone,
        });
      },
    };

    const { Request, Response } = OAuth2Server;
    const fixRequest = {
      ...request,
      body: {
        ...data,
        response_type: 'code',
        grant_type: 'authorization_code',
      },
    };
    const authorizationCode = await this.oAuth2Server.authorize(
      new Request(fixRequest),
      new Response(response),
      authenticateHandler,
    );

    return response.json(authorizationCode);
  }

  /**
   * Get token  code
   * @param req
   * @param res
   * @param data
   */
  @Post('token')
  async token(@Req() req: Request, @Res() res: Response, @Body() data: any) {
    if (data.grant_type === 'authorization_code' && !data['user_id']) {
      throw new BadRequestException(
        undefined,
        'Missing require param `user_id`',
      );
    }

    const { Request, Response } = OAuth2Server;
    const fixRequest = { ...req, body: { ...data } };
    const token = await this.oAuth2Server.token(
      new Request(fixRequest),
      new Response(res),
    );

    if (
      data.grant_type === 'authorization_code' &&
      token.userId.toString() !== data['user_id']
    ) {
      throw new BadRequestException(undefined, 'Authorization Code invalid');
    }

    return res.json(token);
  }
  @UseGuards(LocalAuthGuard)
  @Get('test-local-auth')
  async testLocalAuth(): Promise<string> {
    return 'OK';
  }

  @UseGuards(OAuthTokenGuard)
  @Get('test-token-auth')
  async testTokenAuth(): Promise<string> {
    return 'OK';
  }
}
