import { SchemaFactory } from '@nestjs/mongoose';
import { Post } from 'libs/test-graphql/models';

export const PostSchema = SchemaFactory.createForClass(Post);
