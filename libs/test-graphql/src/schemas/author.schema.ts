import { SchemaFactory } from '@nestjs/mongoose';
import { Author } from 'libs/test-graphql/models';

export const AuthorSchema = SchemaFactory.createForClass(Author);
