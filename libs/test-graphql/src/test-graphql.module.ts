import { Module } from '@nestjs/common';
import { TestGraphqlService } from './test-graphql.service';
import { MongooseModule } from '@nestjs/mongoose';
import { Author, Post } from 'libs/test-graphql/models';
import { AuthorSchema, PostSchema } from 'libs/test-graphql/schemas';
import { RESOLVERS } from 'libs/test-graphql/resolvers';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Author.name, schema: AuthorSchema }]),
    MongooseModule.forFeature([{ name: Post.name, schema: PostSchema }]),
  ],
  providers: [TestGraphqlService, ...RESOLVERS],
  exports: [TestGraphqlService],
})
export class TestGraphqlModule {}
