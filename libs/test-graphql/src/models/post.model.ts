import { Field, Int, ObjectType } from '@nestjs/graphql';
import { Document } from 'mongoose';
import { Prop } from '@nestjs/mongoose';

@ObjectType()
export class Post extends Document {
  @Field(type => Int)
  @Prop({
    type: Number,
  })
  id: number;

  @Field()
  @Prop({
    type: String,
  })
  title: string;

  @Field(type => Int, { nullable: true })
  @Prop({
    type: Number,
  })
  votes?: number;
}
