import { Field, Int, ObjectType } from '@nestjs/graphql';
import { Post } from './post.model';
import { Document, Schema } from 'mongoose';
import { Prop } from '@nestjs/mongoose';

@ObjectType({ description: 'Author model' })
export class Author extends Document {
  @Field(type => Int)
  @Prop({
    type: Number,
    required: true,
  })
  id: number;

  @Field({ nullable: true })
  @Prop({
    type: String,
  })
  firstName?: string;

  @Field({ nullable: true })
  @Prop({
    type: String,
  })
  lastName?: string;

  @Field(type => [Post])
  @Prop({
    type: [Schema.Types.Mixed],
    required: true,
  })
  posts: Post[];
}
