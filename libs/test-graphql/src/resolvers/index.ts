import { AuthorsResolver } from 'libs/test-graphql/resolvers/authors.resolver';

export * from './authors.resolver';

export const RESOLVERS = [AuthorsResolver];
