import { Author } from 'libs/test-graphql/models/author.model';
import {
  Args,
  Int,
  Parent,
  Query,
  ResolveField,
  Resolver,
} from '@nestjs/graphql';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Post } from 'libs/test-graphql/models';

@Resolver(of => Author)
export class AuthorsResolver {
  constructor(@InjectModel(Author.name) private authorModel: Model<Author>) {}

  @Query(returns => Author, { name: 'author', nullable: true })
  async author(@Args('id', { type: () => Int }) id: number) {
    return this.authorModel.find({ id });
  }

  @ResolveField('posts', returns => [Post], {
    nullable: 'itemsAndList',
  })
  async posts(@Parent() author: Author) {
    const { id } = author;
    return [
      {
        id: 1,
        title: 'title1',
      },
    ];
  }
}
