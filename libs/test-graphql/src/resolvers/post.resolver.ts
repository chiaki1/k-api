import { Args, Int, Query, Resolver } from '@nestjs/graphql';
import { Post } from 'libs/test-graphql/models';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

@Resolver(of => Post)
export class AuthorsResolver {
  constructor(@InjectModel(Post.name) private postModel: Model<Post>) {}

  @Query(returns => Post, { name: 'post' })
  async post(@Args('id', { type: () => Int }) id: number) {
    return this.postModel.findById(id);
  }
}
