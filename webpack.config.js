// webpack.config.js
const webpack = require('webpack');
const npm_package = require('./package.json');

module.exports = (config) => {
  const ignorePlugin = new webpack.IgnorePlugin({
    checkResource(resource) {
      const lazyImports = [
        '@nestjs/microservices',
        // ADD THIS
        '@nestjs/microservices/microservices-module',
        '@nestjs/websockets',
        // AND THIS
        '@nestjs/websockets/socket-module',
        '@nestjs/platform-express',
        'cache-manager',
        'class-validator',
        'class-transformer',
      ];

      if (!lazyImports.includes(resource)) {
        return false;
      }
      try {
        require.resolve(resource);
      } catch (err) {
        return true;
      }
      return false;
    },
  });

  config.plugins = config.plugins.concat(ignorePlugin);
  config.resolve = Object.assign(config.resolve, {
    alias: npm_package._moduleAliases || {},
  });
  return config;
};
