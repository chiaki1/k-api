// Bởi vì serverless không hiểu library của nestjs do đó sẽ sinh ra lỗi không hiểu @libs/... là gì
require('module-alias/register');

import { Context, Handler } from 'aws-lambda';
import { Server } from 'http';
import { createServer, proxy } from 'aws-serverless-express';
import { eventContext } from 'aws-serverless-express/middleware';

import { NestFactory } from '@nestjs/core';
import { ExpressAdapter } from '@nestjs/platform-express';
import { AppModule } from './app.module';
import { PCMSProxyMiddleware } from './middleware/pcms-proxy.middleware';

const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');

if (!mongoose) {
  throw Error('could not load mongo');
}
// NOTE: If you get ERR_CONTENT_DECODING_FAILED in your browser, this
// is likely due to a compressed response (e.g. gzip) which has not
// been handled correctly by aws-serverless-express and/or API
// Gateway. Add the necessary MIME types to binaryMimeTypes below
const binaryMimeTypes: string[] = ['application/json'];

let cachedServer: Server;

// Create the Nest.js server and convert it into an Express.js server
async function bootstrapServer(): Promise<Server> {
  if (!cachedServer) {
    /*
     * If you were developing an Express API, for example, you could use the package serverless-http to create your entry point.
     * With a NestJS app, it is a bit harder though, the package is not working currently with NestJS framework.
     * As you may know, under the hood, Nest makes use of Express by default.
     * To create a Serverless entry point with Nest you will need full control of the express lifecycle.
     * You need to use the function NestFactory to do so.
     * */
    const expressApp = express();
    const nestApp = await NestFactory.create(
      AppModule,
      new ExpressAdapter(expressApp),
      {
        cors: true,
      },
    );

    /* Setup proxy for app because we can not use it with bodyParser options*/
    nestApp.use('*', cors());
    nestApp.use('*', new PCMSProxyMiddleware().factory());

    // Setup Aws serverless express
    nestApp.use(eventContext());

    await nestApp.init();
    cachedServer = createServer(expressApp, undefined, binaryMimeTypes);
  }
  return cachedServer;
}

// Export the handler : the entry point of the Lambda function
export const handler: Handler = async (event: any, context: Context) => {
  cachedServer = await bootstrapServer();
  return proxy(cachedServer, event, context, 'PROMISE').promise;
};
