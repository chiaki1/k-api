import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { PCMSProxyMiddleware } from './middleware/pcms-proxy.middleware';

const cors = require('cors');
declare const module: any;

async function bootstrap() {
  /*TODO: fix middleware error by passing option { bodyParser: false }*/
  const app = await NestFactory.create(AppModule, {
    cors: true,
  });

  /* Setup proxy for app because we can not use it with bodyParser options*/
  app.use('*', cors());
  app.use('*', new PCMSProxyMiddleware().factory());

  await app.listen(2005);

  if (module.hot) {
    module.hot.accept();
    module.hot.dispose(() => app.close());
  }
}

bootstrap();
