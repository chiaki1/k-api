import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response } from 'express';
import { createProxyMiddleware, Options } from 'http-proxy-middleware';

const originalOptions: Options = {
  // TODO: need to resolve target by setting or domain name
  target: process.env.PCMS_URL,
  changeOrigin: true, // needed for virtual hosted sites
  ws: true, // proxy websockets
  pathRewrite: {
    '^/api/old-path': '/api/new-path', // rewrite path
    '^/api/rest': '/rest', // remove base path
    '^/proxy/pcms': '', // remove base path
    '^/proxy/pcms/graphql': 'graphql', // remove base path
    '^/dev/proxy/pcms/graphql': 'graphql', // remove base path
    '^/production/proxy/pcms/graphql': 'graphql', // remove base path
    '^/staging/proxy/pcms/graphql': 'graphql', // remove base path
  },
  secure: false,
  onProxyReq: (proxyReq, req, res) => {
    console.log(
      `[Global Functional Middleware]: Proxying ${req.method} request originally made to '${req.originalUrl}'...`,
    );
  },
  router: {
    // TODO: config for development
    // when request.headers.host == 'dev.localhost:3000',
    // override target 'http://www.example.org' to 'http://localhost:8000'
    // 'localhost:3000': 'https://pcms-stg.yutang.vn',
  },
  onProxyRes: (proxyRes, req, res) => {
    proxyRes.headers['Access-Control-Allow-Credentials'] = 'true'; // add new
    proxyRes.headers['Access-Control-Allow-Headers'] = '*'; // add new header
    proxyRes.headers['Access-Control-Allow-Origin'] = '*'; // add new header
    // delete proxyRes.headers['x-removed']; // remove header from response
  },
};

@Injectable()
export class PCMSProxyMiddleware implements NestMiddleware {
  protected _proxy = createProxyMiddleware(['/proxy/pcms/**'], originalOptions);

  use(req: Request, res: Response, next: Function) {
    this._proxy(req, res, next as any);
  }

  factory() {
    return (req: Request, res: Response, next: Function) => {
      this._proxy(req, res, next as any);
    };
  }
}
