import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { TestMongodbModule } from '@libs/test-mongodb';
import { OAuthServerModule } from '@libs/oAuth-server';
import { GraphQLModule } from '@nestjs/graphql';
import { TestGraphqlModule } from 'libs/test-graphql';
const path = require('path');

const nodeEnv = process.env.NODE_ENV;

if (!nodeEnv) {
  throw new Error('Please define `NODE_ENV`');
}

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: ['.env.' + nodeEnv, '.env'],
    }),
    MongooseModule.forRoot(
      'mongodb://kApi:kApiPassword@13.250.98.97:27017/k-api',
    ),
    GraphQLModule.forRoot({
      autoSchemaFile: path.join(process.cwd(), 'src/schema.gql'),
      sortSchema: true,
    }),
    OAuthServerModule,
    TestMongodbModule,
    TestGraphqlModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
